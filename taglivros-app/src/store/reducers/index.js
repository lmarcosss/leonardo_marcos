import * as types from "../constants/ActionTypes";
const initialState = {
  books: [],
  selectedBook: null
};
const books = (state = initialState, action) => {
  switch (action.type) {
    case types.RECEIVE_BOOKS:
      return {
        ...state,
        books: action.books
      };
    case types.SELECT_BOOK:
      return {
        ...state,
        selectedBook: action.selectedBook
      };
    default:
      return state;
  }
};

export { books as reducers };
