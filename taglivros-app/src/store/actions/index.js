import booksApi from "../../api/books";
import * as types from "../constants/ActionTypes";

const receiveBooks = books => ({
  type: types.RECEIVE_BOOKS,
  books: books.results
});

const getAllBooks = () => dispatch => {
  booksApi.getBooks(books => {
    dispatch(receiveBooks(books));
  });
};

const selectBook = selectedBook => ({
  type: types.SELECT_BOOK,
  selectedBook
});

export const booksActions = {
  getAllBooks,
  selectBook
};
