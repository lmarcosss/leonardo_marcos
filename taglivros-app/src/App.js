import React, { Component } from "react";
import "./index.css";
import { Switch, Route } from "react-router-dom";

import ContainerBooks from "./pages/ContainerBooks";
import Book from "./pages/Book";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route path="/" exact component={ContainerBooks} />
          <Route path="/book/:id" component={Book} />
        </Switch>
      </div>
    );
  }
}

export default App;
