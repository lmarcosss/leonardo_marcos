import React, { Component } from "react";
import { Link } from "react-router-dom";
import taglogo from "../../public/images/logo.png";
import "./styles.css";
export default class NavBar extends Component {
  render() {
    return (
      <div className="navbar">
        <div className="navbar-header">
          <img className="tag-logo" src={taglogo} alt="taglogo" />

          <Link className="nav-link" to={this.props.link}>
            Home
          </Link>
        </div>
      </div>
    );
  }
}
