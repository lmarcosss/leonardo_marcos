import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./styles.css";
export default class index extends Component {
  sortBooksToMoreNew() {
    let books = this.props.books;
    let length = books.length;
    for (let i = 0; i < length - 1; i++) {
      for (let j = 0; j < length - 1; j++) {
        if (new Date(books[j].edition) < new Date(books[j + 1].edition)) {
          let temp = books[j];
          books[j] = books[j + 1];
          books[j + 1] = temp;
        }
      }
    }
    return books;
  }

  renderList() {
    return this.sortBooksToMoreNew().map(book => (
      <Link
        to={`book/${book.objectId}`}
        className="book-content"
        key={book.objectId}
      >
        <div className="book-content-img">
          <img className="book-img" src={book.cover.url} alt={book.name} />
        </div>
        <div className="book-content-descriptions">
          <h3 className="book-title">{book.name}</h3>
          <div className="secundary-listBooks book-author">
            Autor: {book.author}
          </div>
          <div className="secundary-listBooks book-isbn">Isbn: {book.isbn}</div>
          <div className="secundary-listBooks book-edition">
            Edição: {book.edition}
          </div>
        </div>
      </Link>
    ));
  }
  render = () => this.renderList();
}
