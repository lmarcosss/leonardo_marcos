import axios from "axios";
const link = "http://localhost:3001";

export default class GoodReadsService {
  static getBookByIsbn(isbn) {
    return axios.get(`${link}/book/${isbn}`).then(rating => {
      return rating.data;
    });
  }
}
