import _books from "./livros.json";

const TIMEOUT = 100;

export default {
  getBooks: (cb, timeout) => setTimeout(() => cb(_books), timeout || TIMEOUT)
};
