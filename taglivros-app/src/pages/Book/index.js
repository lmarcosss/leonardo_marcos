import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { booksActions } from "../../store/actions";

import "./styles.css";
import goodReads from "../../public/images/goodreads.png";
import tagLivros from "../../public/images/logo.png";
import NavBar from "../../components/NavBar";
import Service from "../../services/GoodReadsService";
import Loading from "../../components/Loading";

class Book extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };

    setTimeout(() => {
      this.getBook();
      setTimeout(() => {
        this.setState({ loading: false });
      }, 3000);
    }, 500);
  }

  getBook() {
    if (this.props.books) {
      let book = this.props.books.find(
        book => book.objectId === this.props.match.params.id
      );
      if (book) {
        book = { ...book, ratingsCountGoodReads: undefined };
        this.props.selectBook(book);

        Service.getBookByIsbn(book.isbn).then(result => {
          book = {
            ...book,
            averageRatingGoodReads: result.average_rating._text,
            ratingsCountGoodReads: result.ratings_count._text
          };
          this.props.selectBook(book);
        });
      }
    }
  }

  getRating(totalRatings, numRatings) {
    let result = Math.floor((totalRatings / numRatings) * 100) / 100;
    return result.toFixed(2);
  }

  getMediaTAG(book) {
    return this.getRating(book.totalRatings, book.numRatings);
  }

  renderAverageRatingGoodReads(avarageRatings, ratingsCount) {
    if (avarageRatings && ratingsCount) {
      return (
        <div className="gr-content">
          <img className="img-goodreads" src={goodReads} alt="" />
          <div className="goodreads">
            {avarageRatings}
            <p>{ratingsCount}</p>
          </div>
        </div>
      );
    }
    return <div />;
  }

  renderBook(book) {
    if (book) {
      return (
        <div className="content-book">
          <div className="container-img-book">
            <img className="img-book" src={book.cover.url} alt={book.name} />
          </div>
          <div className="container-informations">
            <div className="informations-book">
              <div className="descriptions-book">
                <h1 className="name-book">{book.name}</h1>
                <h4 className="author-book">{book.author}</h4>
                <div className="secundary edition-book">
                  Edição: {book.edition}
                </div>
                <div className="secundary curator-book">
                  Curador: {book.curator}
                </div>
                <div className="secundary pages-book">
                  Páginas: {book.pages}
                </div>
              </div>
              <div className="average-book">
                <div className="line">
                  <div>Média:</div>
                  <p>Nº Avaliações:</p>
                </div>
                <div className="container-average">
                  <div className="tag-content">
                    <img className="img-taglivros" src={tagLivros} alt="" />
                    <div className="taglivros">
                      {this.getMediaTAG(book)}
                      <p>{book.totalRatings}</p>
                    </div>
                  </div>
                  {this.renderAverageRatingGoodReads(
                    book.averageRatingGoodReads,
                    book.ratingsCountGoodReads
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return <div />;
    }
  }

  render() {
    return (
      <div className="container-book">
        <Loading showLoader={this.state.loading} />
        <NavBar link="/" />
        {this.renderBook(this.props.selectedBook)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  books: state.books,
  selectedBook: state.selectedBook
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      selectBook: booksActions.selectBook
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Book);
