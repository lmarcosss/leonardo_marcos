import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import "./styles.css";

import { booksActions } from "../../store/actions";
import NavBar from "../../components/NavBar";
import ListBooks from "../../components/ListBooks";
class ContainerBooks extends Component {
  render() {
    return (
      <div className="book-container">
        <NavBar link="/" />
        <div className="list-books">
          <ListBooks books={this.props.books} />
        </div>
        <div className="footer">
          <p>Nome: Leonardo dos Santos Marcos</p>
          <p>Data de Desenvolvimento: 01/12/18 até 10/12/18</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  books: state.books
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllBooks: booksActions.getAllBooks
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContainerBooks);
